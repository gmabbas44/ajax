<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css">
</head>

<body>
    <div class="container-fluid m-2">
        <div class="row">
            <div class="col-md-6">
                <div id="success-alert"></div>
                <form method="post">
                    <div class="form-group">
                        <label for="name">Name </label>
                        <input type="text" class="form-control" name="name" id="name" placeholder="Enter name">
                        <small id="errname" class="text-danger"></small>
                    </div>
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="email" class="form-control" name="email" id="email" placeholder="Enter email">
                        <small id="erremail" class="text-danger"></small>
                    </div>
                    <div class="form-group">
                        <label for="password">Password</label>
                        <input type="password" class="form-control" name="password" id="password" placeholder="Enter your password">
                        <small id="errpassword" class="text-danger"></small>
                    </div>
                    <button type="submit" class="btn btn-primary" id="submit-btn">Save</button>
                </form>
            </div>
            <div class="col-md-6"></div>
        </div>
    </div>


    <script>
        let submit_btn = document.getElementById('submit-btn');
        submit_btn.addEventListener('click', function(e) {
            e.preventDefault();
            let name, email, password, errers = [];

            name = document.getElementById('name').value;
            email = document.getElementById('email').value;
            password = document.getElementById('password').value;

            if (name === '') {
                errers.push('Name');
                document.getElementById('errname').textContent = 'Name must not be empty';
            }
            if (email === '') {
                errers.push('Email');
                document.getElementById('erremail').textContent = 'Name must not be empty';
            }
            if (password === '') {
                errers.push('Password');
                document.getElementById('errpassword').textContent = 'Name must not be empty';
            }

            // if( errers.length !== 0 ){
            //     for( let i in errers ) {
            //         document.getElementById(`'${errers[i]}'`).innerHTML =  errers[i] +' must not be empty';
            //     }
            // }

            if (errers.length === 0) {
                // create xhr Object
                const xhr = new XMLHttpRequest;

                // initialize value 
                xhr.open('POST', 'insert.php', true);

                // set request header
                xhr.setRequestHeader('Content-type', 'application/json');

                // handel request
                xhr.onload = () => {
                    if (xhr.status === 200) {
                        console.log();
                        document.getElementById('success-alert').innerHTML = `<div class="alert alert-warning">${xhr.responseText}</div>`;
                    } else {
                        console.log('Server Error');
                    }
                }

                // js Object
                const myData = { name : name, email : email, password : password } ;
                // convert json string
                const jsonData = JSON.stringify(myData);
                // send data 
                xhr.send(jsonData);

            }



        })
    </script>
</body>

</html>