<?php

    $con = mysqli_connect('localhost','root', '','cstistd');
    
    $sql = "SELECT * FROM  artist";
    $res = mysqli_query($con, $sql);
    
    $artists = mysqli_fetch_all($res, MYSQLI_ASSOC);

    



?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
</head>
<body>
    <div class="container">
    <form>
        <div class="form-row">
 
            <div class="form-group col-md-4">
                <label for="artist">Artist</label>
                <select id="artist" class="form-control" onchange="myfun(this.value)">

                    <option selected>Choose...</option>
                    <?php foreach ($artists as $artist): ?>
                    <option value="<?= $artist['artist_id']; ?>"><?= $artist['artist_name']; ?></option>
                    <?php endforeach;?>

                </select>
            </div>
            <div class="form-group col-md-4">
                <label for="painting">Painting</label>
                <select id="painting" class="form-control" >
                <option selected>Choose...</option>
                </select>
            </div>
        </div>
        <button type="submit" class="btn btn-primary">Sign in</button>
    </form>
    </div>


    <script>
        function myfun(artist_id){
            $.ajax({
                url : 'painting.php',
                type : 'POST',
                data : {
                    artist_id : artist_id
                },

                success : function(result){
                    $('#painting').html(result);
                }
            });
        }
    </script>
</body>
</html>