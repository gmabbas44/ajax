<?php



    $update_id = $_POST['update_id'];
    $con = mysqli_connect('localhost','root', '','ajax');
    $sql = "SELECT * FROM user_info WHERE id = $update_id";
    $result = mysqli_query($con, $sql);
    $users = mysqli_fetch_assoc($result);


?>

<form action="" method="POST">
    <div class="modal-body">
        <input id="updateid" name="updateid" type="hidden" value="<?= $update_id; ?>">
        <div class="form-group">
            <label for="fullName">Full name</label>
            <input name="fullName" value="<?= $users['name']; ?>" type="text" class="form-control" id="fullName" placeholder="Enter your full name" required>
        </div>
        <div class="form-group">
            <label for="username">User name</label>
            <input name="username" value="<?= $users['user_name']; ?>" type="text" class="form-control" id="username" placeholder="Enter your user name" required>
        </div>
        <div class="form-group">
            <label for="mobile">Mobile</label>
            <input name="mobile" value="<?= $users['mobile']; ?>" type="number" class="form-control" id="mobile" placeholder="Enter your mobile number" required>
        </div>
        <div class="form-group">
            <label for="email">Email</label>
            <input name="email" value="<?= $users['email']; ?>" type="email" class="form-control" id="email" placeholder="Enter your Email" required>
        </div>
        <div class="form-group">
            <label for="pass">Passwor</label>
            <input name="pass" value="<?= $users['password']; ?>" type="password" class="form-control" id="pass" placeholder="Enter your password" required >
        </div>
    </div>
    <div class="modal-footer">
        <button onclick="updateData()" type="submit" name="update" class="btn btn-primary">Save</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
    </div>
</form>