<?php 
    if(isset($_POST['read'])):
        $con = mysqli_connect('localhost','root', '','ajax');
        $sql = "SELECT * FROM user_info";
        $result = mysqli_query($con, $sql);
        $users = mysqli_fetch_all($result, MYSQLI_ASSOC);
        $i = 1;
?>

<table class="table table-bordered table-sm">
    <thead class="table-primary">
        <th>Id</th>
        <th>Name</th>
        <th>User Name</th>
        <th>Email</th>
        <th>Mobile</th>
        <th>action</th>
    </thead>
    <tbody>
    <?php foreach($users as $user): ?>
        <tr>
            <td><?= $i; ?></td>
            <td><?= $user['name'] ; ?></td>
            <td><?= $user['user_name'] ; ?></td>
            <td><?= $user['email'] ; ?></td>
            <td><?= $user['mobile'] ; ?></td>
            <td>
            <button type="submit" id="update" onclick="update(<?= $user['id'] ; ?>)" class="btn btn-primary btn-sm update">
            Edit
            </button>
                <button onclick="deleteData(<?= $user['id'] ; ?>)" class="btn btn-danger btn-sm">Delete</button>
            </td>
        </tr>
    <?php $i++; endforeach; ?>
    </tbody>
</table>

<?php

else:
    echo "<h2> No data here</h2>";
endif;