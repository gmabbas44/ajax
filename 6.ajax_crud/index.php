<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
</head>
<body>
    <div class="container">
        <h2 class="text-center text-success">ajax crud operation</h2>
        <div class="d-flex justify-content-end">
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addNew">
            Add New
            </button>
        </div>
        <!-- Button trigger modal -->

        <!-- Insert Modal -->
        <div class="modal fade" id="addNew" tabindex="-1" role="dialog" aria-labelledby="addNewLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="addNewLabel">Modal title</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form action="" method="POST">
                        <div class="modal-body">
                            <div class="form-group">
                                <label for="fullName">Full name</label>
                                <input name="fullName" type="text" class="form-control" id="fullName" placeholder="Enter your full name" required>
                            </div>
                            <div class="form-group">
                                <label for="username">User name</label>
                                <input name="username" type="text" class="form-control" id="username" placeholder="Enter your user name" required>
                            </div>
                            <div class="form-group">
                                <label for="mobile">Mobile</label>
                                <input name="mobile" type="number" class="form-control" id="mobile" placeholder="Enter your mobile number" required>
                            </div>
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input name="email" type="email" class="form-control" id="email" placeholder="Enter your Email" required>
                            </div>
                            <div class="form-group">
                                <label for="pass">Passwor</label>
                                <input name="pass" type="password" class="form-control" id="pass" placeholder="Enter your password" required >
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button onclick="addRecord()" type="submit" name="submit" class="btn btn-primary">Save</button>
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="modal fade" id="upadate" tabindex="-1" role="dialog" aria-labelledby="upadateLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="upadateLabel">Data Update</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <div id="updatemodel">
                        <!-- ajax data update by update.php -->
                    </div>
                </div>
            </div>
        </div>

        <h2>All Record</h2>

        <div id="showRecord">
            <!-- show record -->
            <!-- from read.php -->
        </div>  
    </div>
    
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>


<script>

$(document).ready(function(){
    readRecord();

});

function update(id){
    $.ajax({
        url : "update.view.php",
        type: 'post',
        data : {
            update_id : id
        },
        success : function(data, status){
            $('#updatemodel').html(data);
            $("#upadate").modal('show');
        }
    });
  
}

function updateData(){
    var id          = $('#updateid').val();
    var fullName    = $('#fullName').val();
    var username    = $('#username').val();
    var email       = $('#email').val();
    var mobile      = $('#mobile').val();
    var pass        = $('#pass').val();

    $.ajax({
        url : 'update.php',
        type : 'post',
        data :{
            id : id,
            fullName : fullName,
            username : username,
            email : email,
            mobile : mobile,
            pass : pass
        },
        success: function(data, status){
            console.log(fullName);
            //readRecord();
        }
    });
}

function readRecord(){
    var readRecord = "readRecord";

    $.ajax({
        url : "read.php",
        type : "post",
        data : { 
            read : readRecord 
        },
        success: function(data, status){
            $('#showRecord').html(data);
        }
    });
}
// function for addrecord
function addRecord(){

    var fullName    = $('#fullName').val();
    var username    = $('#username').val();
    var email       = $('#email').val();
    var mobile      = $('#mobile').val();
    var pass        = $('#pass').val();

    $.ajax({
        url : "insert.php",
        type : "post",
        data : {
            fullName    : fullName,
            username    : username,
            email       : email,
            mobile      : mobile,
            pass        : pass
        },
        success: function(data, status){
            readRecord();
        }
    });
    //alert(fullName +' '+ username +' '+ email+' '+' '+ mobile+ ' ' +pass);
}


function deleteData(id){
    var conf = confirm("Are You Delete confirm?");
    if(conf == true){
        var id = id;
        //alert(id);

        $.ajax({
            url : 'delete.php',
            type : 'post',
            data : {
                deleteId : id
            },
            success : function(data, status){
                readRecord();
            }
        });
    }
    

}



</script>
</body>
</html>