<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script>
        $(document).ready(function() {
            $('.get_data').click(function() {
                /*
                this is a get method
                $.get(URL, callback)

                 $get('a html or php file', (functin(html data from url file, success of filed){
                    $('put data').html(html data from url file);
                    alert(success of filed or any messages);
                })
                */
                $.get('load.php', function(data, status) {
                    $('#test').html(data);
                    alert('This is done');
                });
            });

            $('.post_data').click(function() {
                /*
                    this is a post method
                    $.post(URL, data, callback)

                    $get('a html or php file',data from html input field, (functin(html data from url file, success of filed){
                        $('put data').html(html data from url file);
                        alert(success of filed or any messages);
                    })
                */
                $.post('post.php', {
                    name: 'Gm Abbas Uddin',
                    roll: 382633
                }, function(data, status) {
                    $('#test').html(data);
                    alert(status);
                });
            });
        });
    </script>
</head>

<body>
    <div class="container">
        <p id="test">This is html contain</p>
        <button class="btn btn-primary get_data">Get data</button>
        <button class="btn btn-primary post_data">Post data</button>
    </div>


</body>

</html>