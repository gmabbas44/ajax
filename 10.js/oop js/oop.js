'use strict';

class Account {

    // public poparty
    locale = navigator.language;
    owner;
    currency;

    // protected
    _movement = [];

    // pirvate poperty
    #pin ;
    
    constructor( owner, currency, pin ) {
        this.owner = owner;
        this.currency = currency;
        this.#pin = pin;
        // this.local = navigator.languages;
    }

    // public method 
    deposit( val ) {
        this._movement.push( val );
        return this;
    }

    getMovement() {
        return this._movement;
    }

    // public method 
    withdraw( val ) {
        this.deposit(-val);
        return this;
    }

    // private method
    #pinNo () {
        return this.#pin;
        return this;
    }
    // static method 
    static helper() {
        console.log( 'Hello! this is helper' );
        return this;
    }

}


const acc1 = new Account('Gm Abbas Uddin', 'BDT', '1234');
const acc2 = new Account('Rubel Amin', 'BDT', '1234');
acc1.deposit(522);
acc1.deposit(4522);

// method chaining 
acc1.deposit(100).withdraw(200).deposit(5000);

console.log( acc1.getMovement() );


console.log(acc1.locale);
// console.log( acc1.#pinNO() );

// Account.helper() ;

