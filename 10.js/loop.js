
let numbers = [200, 100, 300, 500, 600, 800];

for( let i in numbers ) {
    console.log(numbers[i]);
}

for( let number of numbers ) {
    console.log(number);
}

numbers.forEach( (number, i, arr) => {
    console.log(`${i + 1} is ${number} ${arr}`);
});