/**
 * @param {HTMLTableElement} table the table to sort
 * @param {Number} column The index of the column to sort 
 * @param {Boolean} asc Determines if the sorting will be in asceding
 */
const sortTableByColumn = (table, column, asc = true) => {
  const dirModifier = asc ? 1 : -1
  const tBody = table.tBodies[0]
  const rows = Array.from(tBody.querySelectorAll('tr'))

  // Sort each row
  const sortedRows = rows.sort((a, b) => {
    const aColText = a.querySelector(`td:nth-child(${column + 1})`).textContent.trim()
    const bColText = b.querySelector(`td:nth-child(${column + 1})`).textContent.trim()
    return aColText > bColText ? (1 * dirModifier) : (-1 * dirModifier)
  })

  // Remove all existing TRs from table
  while (tBody.firstChild) {
    tBody.removeChild(tBody.firstChild)
  }

  // Re-add tbe newly sorted rows
  tBody.append(...sortedRows)

  // Remember how the column currently sorted
  table.querySelectorAll('th').forEach(th => th.classList.remove('th-sort-asc', 'th-sort-desc'))
  table.querySelector(`th:nth-child(${column + 1})`).classList.toggle('th-sort-asc', asc)
  table.querySelector(`th:nth-child(${column + 1})`).classList.toggle('th-sort-desc', !asc)

}

const table = document.querySelector('table')

const createDiv = (height) => {
  const div = document.createElement('div')
  div.style.top = 0
  div.style.right = 0
  div.style.width = '10px'
  div.style.position = 'absolute'
  div.style.cursor = 'col-resize'
  div.style.userSelect = 'none'
  div.style.height = height + 'px'
  return div
}

const setListeners = (div) => {
  let pageX, curCol, nxtCol, curColWidth, nxtColWidth
  div.addEventListener('mousedown', function (e) {
    curCol = e.target.parentElement
    nxtCol = curCol.nextElementSibling
    pageX = e.pageX
    curColWidth = curCol.offsetWidth
    if (nxtCol) nxtColWidth = nxtCol.offsetWidth
  })

  document.addEventListener('mousemove', function (e) {
    if (curCol) {
      let diffX = e.pageX - pageX
      if (nxtCol) nxtCol.style.width = (nxtColWidth - diffX) + 'px'
      curCol.style.width = (curColWidth + diffX) + 'px'
    }
  })

  document.addEventListener('mouseup', function (e) {
    curCol = undefined
    nxtCol = undefined
    pageX = undefined
    nxtColWidth = undefined
    curColWidth = undefined
  })
}

// for resize table
const resizeTableCol = (table) => {
  const row = table.getElementsByTagName('tr')[0]
  const cols = row ? row.children : undefined

  if (!cols) return
  let count = 0
  while (count < cols.length) {
    const div = createDiv(row.offsetHeight)
    cols[count].appendChild(div)
    cols[count].style.position = 'relative'
    setListeners(div)
    count++
  }
}
resizeTableCol(table)

document.querySelectorAll('.table-sortable th').forEach(headerCell => {
  headerCell.addEventListener('click', () => {
    const tableElement = headerCell.parentElement.parentElement.parentElement
    const headerIndex = Array.prototype.indexOf.call(headerCell.parentElement.children, headerCell)
    const currentSortedCol = headerCell.classList.contains('th-sort-asc')
    sortTableByColumn(tableElement, headerIndex, !currentSortedCol)
  })
})