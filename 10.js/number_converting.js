'use strict';

console.log( 3 === 3.00 );
console.log( 2 + 3 === 5 );
console.log( .2 + .3 === .5 );

// conversion
console.log( Number('50') );
console.log( +'30' );

// parse integer convert
console.log( Number.parseInt( '3.25rem' ) );
console.log( Number.parseInt( 'e3.25rem' ) );
console.log( Number.parseFloat( '3.25856' ) );

// convert binary
console.log( Number.parseInt('1110.10', 2))
console.log( Number.parseFloat('1110.11', 2))

// convert octal
console.log( Number.parseInt( '10', 8)); // octal to decimal convert
console.log( Number.parseInt( '10.20', 8)); // octal to decimal convert

// convert hexa decimal to decimal
console.log( Number.parseInt( 'A', 16 ));

// check integer
console.log( `is is ineger ? ` + Number.isInteger(20))

console.log( Number.isNaN('20'));

// checking if value is number
console.log( Number.isFinite('20'));