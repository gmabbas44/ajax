'use strict';

/////////////////////////////////////////////////
/////////////////////////////////////////////////
// BANKIST APP

// Data
const account1 = {
    owner: 'Jonas Schmedtmann',
    movements: [200, 450, -400, 3000, -650, -130, 70, 1300],
    interestRate: 1.2, // %
    pin: 1111,

    movementsDates: [
        '2019-11-18T21:31:17.178Z',
        '2019-12-23T07:42:02.383Z',
        '2020-01-28T09:15:04.904Z',
        '2020-04-01T10:17:24.185Z',
        '2020-05-08T14:11:59.604Z',
        '2020-07-26T17:01:17.194Z',
        '2020-07-28T23:36:17.929Z',
        '2020-08-01T10:51:36.790Z',
    ],
    currency: 'EUR',
    locale: 'pt-PT', // de-DE
};

const account2 = {
    owner: 'Jessica Davis',
    movements: [5000, 3400, -150, -790, -3210, -1000, 8500, -30],
    interestRate: 1.5,
    pin: 2222,
    movementsDates: [
        '2019-11-18T21:31:17.178Z',
        '2019-12-23T07:42:02.383Z',
        '2020-01-28T09:15:04.904Z',
        '2020-04-01T10:17:24.185Z',
        '2020-05-08T14:11:59.604Z',
        '2020-07-26T17:01:17.194Z',
        '2020-07-28T23:36:17.929Z',
        '2020-08-01T10:51:36.790Z',
    ],
    currency: 'EUR',
    locale: 'pt-PT', // de-DE
};

const account3 = {
    owner: 'Steven Thomas Williams',
    movements: [200, -200, 340, -300, -20, 50, 400, -460],
    interestRate: 0.7,
    pin: 3333,

    movementsDates: [
        '2019-11-18T21:31:17.178Z',
        '2019-12-23T07:42:02.383Z',
        '2020-01-28T09:15:04.904Z',
        '2020-04-01T10:17:24.185Z',
        '2020-05-08T14:11:59.604Z',
        '2020-07-26T17:01:17.194Z',
        '2020-07-28T23:36:17.929Z',
        '2020-08-01T10:51:36.790Z',
    ],
    currency: 'EUR',
    locale: 'pt-PT', // de-DE
};

const account4 = {
    owner: 'Sarah Smith',
    movements: [430, 1000, 700, 50, 90, -150],
    interestRate: 1,
    pin: 4444,
    movementsDates: [
        '2019-11-18T21:31:17.178Z',
        '2019-12-23T07:42:02.383Z',
        '2020-01-28T09:15:04.904Z',
        '2020-04-01T10:17:24.185Z',
        '2020-05-08T14:11:59.604Z',
        '2020-07-26T17:01:17.194Z'
    ],
    currency: 'EUR',
    locale: 'pt-PT', // de-DE
};

const account5 = {
    owner: 'Gm Abbas Uddin',
    movements: [200, 450, -650, -130, 1300],
    interestRate: 1.2, // %
    pin: 5555,
    movementsDates: [
        '2019-11-18T21:31:17.178Z',
        '2019-12-23T07:42:02.383Z',
        '2020-01-28T09:15:04.904Z',
        '2020-04-01T10:17:24.185Z',
        '2020-05-08T14:11:59.604Z',
    ],
    currency: 'USD',
    locale: 'en-US', // de-DE
};

const accounts = [account1, account2, account3, account4, account5];

// Elements
const labelWelcome = document.querySelector('.welcome');
const labelDate = document.querySelector('.date');
const labelBalance = document.querySelector('.balance__value');
const labelSumIn = document.querySelector('.summary__value--in');
const labelSumOut = document.querySelector('.summary__value--out');
const labelSumInterest = document.querySelector('.summary__value--interest');
const labelTimer = document.querySelector('.timer');

const containerApp = document.querySelector('.app');
const containerMovements = document.querySelector('.movements');

const btnLogin = document.querySelector('.login__btn');
const btnTransfer = document.querySelector('.form__btn--transfer');
const btnLoan = document.querySelector('.form__btn--loan');
const btnClose = document.querySelector('.form__btn--close');
const btnSort = document.querySelector('.btn--sort');

const inputLoginUsername = document.querySelector('.login__input--user');
const inputLoginPin = document.querySelector('.login__input--pin');
const inputTransferTo = document.querySelector('.form__input--to');
const inputTransferAmount = document.querySelector('.form__input--amount');
const inputLoanAmount = document.querySelector('.form__input--loan-amount');
const inputCloseUsername = document.querySelector('.form__input--user');
const inputClosePin = document.querySelector('.form__input--pin');

const formatMovementDate = (date, locale) => {
    const calcDaysPassed = (date1, date2) => Math.round(Math.abs(date2 - date1) / (1000 * 60 * 60 * 24));
    const daysPassed = calcDaysPassed(new Date(), date);
    if (daysPassed === 0) return 'Today';
    if (daysPassed === 1) return 'Yesterday';
    if (daysPassed <= 0) return `${daysPassed} days ago`;

    return new Intl.DateTimeFormat(locale).format(date);
}

const formatCrr = (value, locale, currency) => {
    return new Intl.NumberFormat(locale, {
        style: 'currency',
        currency: currency
    }).format(value);
}

const displayMovements = function (acc, sort = false) {
    containerMovements.innerHTML = '';
    const movs = sort
        ? acc.movements.slice().sort((a, b) => a - b)
        : acc.movements.slice().sort((a, b) => b - a);
    // const movs = sort ? movements.slice().sort((a, b) => a - b) : movements;

    movs.forEach((mov, i) => {
        const type = mov > 0 ? 'deposit' : 'withdrawal';
        const date = new Date(acc.movementsDates[i]);
        const displayTime = formatMovementDate(date, acc.locale);
        const formatedMov = formatCrr(mov, acc.locale, acc.currency);
        const html = `
        <div class="movements__row">
          <div class="movements__type movements__type--${type}">${i + 1} ${type}</div>
          <div class="movements__date">${displayTime}</div>
          <div class="movements__value">${formatedMov}</div>
        </div>
        `;
        containerMovements.insertAdjacentHTML('afterbegin', html);
    });
}

// create use name function
const createUsername = (accs) => {
    accs.forEach((acc) => {
        acc.username = acc.owner.toLowerCase()
            .split(' ')
            .map(name => name[0])
            .join('');
    });
}
createUsername(accounts);

// calculate and display final balence
const calcDisplayBalence = account => {
    account.balance = account.movements.reduce((acc, curr) => acc + curr, 0);
    labelBalance.textContent = formatCrr(account.balance, account.locale, account.currency);
};

// calculate display summary
const calcDisplaySummary = (account) => {
    const incomes = account.movements.filter(mov => mov > 0).reduce((acc, mov) => acc + mov, 0);
    labelSumIn.textContent = formatCrr(incomes, account.locale, account.currency);

    const cost = account.movements.filter(mov => mov < 0).reduce((acc, mov) => acc + mov, 0);
    labelSumOut.textContent = formatCrr(Math.abs(cost), account.locale, account.currency);

    const interest = account.movements.filter(mov => mov > 0).map(deposit => (deposit * account.interestRate) / 100).filter((int, i, arr) => int >= 1).reduce((acc, int) => acc + int, 0);
    labelSumInterest.textContent = formatCrr(interest, account.locale, account.currency);
}

const updateUI = acc => {
    // display movements
    displayMovements(acc);

    // diplay balance 
    calcDisplayBalence(acc);

    // diplay summary
    calcDisplaySummary(acc);
};

// Event handler
let currentAccount;

currentAccount = account1;
containerApp.style.opacity = 100;
updateUI(currentAccount);

btnLogin.addEventListener('click', (e) => {
    e.preventDefault();
    currentAccount = accounts.find(acc => acc.username === inputLoginUsername.value);

    if (currentAccount?.pin === Number(inputLoginPin.value)) {
        // display welcome messages
        labelWelcome.textContent = `Welcome back, ${currentAccount.owner.split(' ')[1]}`;

        // visible
        containerApp.style.opacity = 100;

        // create local date time 
        const now = new Date();
        // const day   = `${now.getDate()}`.padStart(2, 0);
        // const month = `${now.getMonth() + 1}`.padStart(2, 0) ;
        // const year  = now.getFullYear();
        // const hour  = now.getHours();
        // const min   = now.getMonth();
        const option = {
            hour: 'numeric',
            minute: 'numeric',
            day: 'numeric',
            month: 'numeric',
            // month: 'long',
            year: 'numeric',
            // weekday: 'long'
        }

        labelDate.textContent = Intl.DateTimeFormat(currentAccount.locale, option).format(now);

        // login input clear
        inputLoginUsername.value = inputLoginPin.value = '';
        inputLoginPin.blur();

        updateUI(currentAccount);
    }
});

btnTransfer.addEventListener('click', (e) => {
    e.preventDefault();
    const amount = Number(inputTransferAmount.value);
    const receiveAcc = accounts.find(acc => acc.username === inputTransferTo.value);

    if (amount > 0 && receiveAcc && currentAccount.balance >= amount && receiveAcc?.username !== currentAccount.username) {
        currentAccount.movements.push(-amount);
        receiveAcc.movements.push(amount);

        // add transfer date 
        currentAccount.movementsDates.push(new Date().toISOString());
        receiveAcc.movementsDates.push(new Date().toISOString());

        updateUI(currentAccount);

        inputTransferAmount.value = inputTransferTo.value = '';
    } else {
        console.log('invalid');
    }
});

btnClose.addEventListener('click', (e) => {
    e.preventDefault();

    if (inputCloseUsername.value === currentAccount.username && Number(inputClosePin.value) === currentAccount.pin) {
        const index = accounts.findIndex(acc => acc.username === currentAccount.username);

        // console.log(index);

        // delete account
        accounts.splice(index, 1);

        // hide UI
        containerApp.style.opacity = 0;

        // clear value 
        inputCloseUsername.value = inputClosePin.value = '';
    }
});

btnLoan.addEventListener('click', (e) => {
    e.preventDefault();
    const amount = Number(inputLoanAmount.value);

    if (amount > 0 && currentAccount.movements.some(mov => mov >= amount * 0.1)) {
        // add movement
        currentAccount.movements.push(amount);

        // add movement time
        currentAccount.movementsDates.push(new Date().toISOString());

        // update UI
        updateUI(currentAccount);
    }
    inputLoanAmount.value = '';
});

// for sorted 
let sorted = false;
btnSort.addEventListener('click', (e) => {
    e.preventDefault();
    displayMovements(currentAccount, !sorted);
    sorted = !sorted;
});




/////////////////////////////////////////////////
/////////////////////////////////////////////////
// LECTURES

const currencies = new Map([
    ['USD', 'United States dollar'],
    ['EUR', 'Euro'],
    ['GBP', 'Pound sterling'],
]);

const movements = [200, 450, -400, 3000, -650, -130, 70, 1300];


/////////////////////////////////////////////////
