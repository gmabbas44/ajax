const filled = document.querySelector('.fill');
const empties = document.querySelectorAll('.empty');

// drag function 
const dragStart = function () {
    // console.log( this );
    filled.className += ' hold';
    setTimeout( () => filled.className = 'invisible', 0);
}
const dragEnd = function () {
    filled.className = 'fill';
}
const dragOver = function (e) {
    e.preventDefault();
}
const dragEnter = function (e) {
    e.preventDefault();
    this.className += ' hovered';
}
const dragLeave = function (e) {
    this.className = 'empty';

}
const dragDrop = function (e) {
    this.className = 'empty';
    this.append(filled);
}


empties.forEach( (empty) => {
    empty.addEventListener('dragover', dragOver);
    empty.addEventListener('dragenter', dragEnter);
    empty.addEventListener('dragleave', dragLeave);
    empty.addEventListener('drop', dragDrop);
});


// fill listerner
filled.addEventListener('dragstart', dragStart);
filled.addEventListener('dragend', dragEnd);


