'use strict';


const movements = [200, 450, -400, 3000, -650, -130, 70, 1300];

const movementsFilter = movements.filter(mov => mov > 0);
// console.log(movementsFilter);


const filter = [];

for (let mov of movements)
    if (mov > 0)
        filter.push(mov);


// console.log(filter);

const totalDepositUSD = movements.filter(mov => mov < 0)
    .map((mov, i, arr) => mov * 1.11)
    .reduce( (acc, curr) => acc + curr);

console.log(totalDepositUSD);
