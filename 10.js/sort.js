'use strict';

let arrString = [ 'a', 'A', 'ac', 'Ab', 'AB', 'abbas', 'Nodi'];

// console.log( arrString.slice() );

// string ascending order
console.log(arrString.sort());
// string ascending order
console.log(arrString.reverse());


const movements = [200, 450, -400, 3000, -650, -130, 70, 1300];

// number ascending order 
console.log(movements.sort( ( a, b ) => a - b ));
console.log(movements.reverse());
// number descending order 
console.log(movements.sort( ( a, b ) => b - a ));
