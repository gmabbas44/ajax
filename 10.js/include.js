'use strict' ;

const movements = [200, 450, -400, 3000, -650, -130, 70, 1300];

// this is function are return Boolean 
/*
    * includes
    * some 
    * every
*/
console.log(movements);
// equality
const add = movements.includes(-130);
console.log(add);

// condition
const anydop = movements.some( mov => mov > 500 );
console.log(anydop);

// every
const anydep = movements.every( mov => mov > 0 );
console.log(anydep);

const dry = mov => mov > 0;
console.log( movements.some(dry));
console.log( movements.every(dry));
console.log( movements.filter(dry));



