
const arr = [1, 2, 3, 4, 5, 6, 7, 8, 9]
let evn_sum = 0
for (let i = 0; i < arr.length; i++) {
  if (arr[i] % 2 === 0) {
    console.log(arr[i])
    evn_sum += arr[i]
  }
}
console.log('evn sum: ', evn_sum)

let odd_sum = 0
for (let i = 0; i < arr.length; i++) {
  if (arr[i] % 2 === 1) {
    console.log(arr[i])
    odd_sum += arr[i]
  }
}
console.log('odd sum', odd_sum)
console.log(arr)

arr.splice(3, 2, 90, 34) // remove from index 3 value and add 90, 34
console.log(arr)
