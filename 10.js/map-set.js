'use strict' ;


const currencies = new Map([
    ['BDT', 'Bangladesh'],
    ['USD', 'United State'],
    ['EUR', 'Euro']
]);

currencies.forEach(function( value, key, map ) {
    console.log(`${key} : ${value}`);
});

const currenciesUnite = new Set(['BDT', 'USD', 'EUR', 'URO']);

console.log(currenciesUnite);

currenciesUnite.forEach(function( value, key, map ) {
    console.log(`${key} : ${value}`);
})


// const movements = [200, 450, -400, 3000, -650, -130, 70, 1300];

// const usd = 1.1;
// const movementsUSD = movements.map( (mov) => mov * usd );
// console.log(movementsUSD);

