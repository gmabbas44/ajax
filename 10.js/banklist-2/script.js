'use strict';

///////////////////////////////////////
// Modal window

const modal = document.querySelector('.modal');
const overlay = document.querySelector('.overlay');
const btnCloseModal = document.querySelector('.btn--close-modal');
const btnsOpenModal = document.querySelectorAll('.btn--show-modal');
const btnScrollTo = document.querySelector('.btn--scroll-to');
const section1 = document.querySelector('#section--1');

const openModal = function () {
	modal.classList.remove('hidden');
	overlay.classList.remove('hidden');
};

const closeModal = function () {
	modal.classList.add('hidden');
	overlay.classList.add('hidden');
};

for (let i = 0; i < btnsOpenModal.length; i++)
	btnsOpenModal[i].addEventListener('click', openModal);

btnCloseModal.addEventListener('click', closeModal);
overlay.addEventListener('click', closeModal);

document.addEventListener('keydown', function (e) {
	if (e.key === 'Escape' && !modal.classList.contains('hidden')) {
		closeModal();
	}
});

btnScrollTo.addEventListener('click', (event) => {
	const scroll = section1.getBoundingClientRect()
	// console.log(scroll);
	section1.scrollIntoView({
		behavior: 'smooth'
	});
});

const $ = (t) => { document.querySelector(t); return this };


// smooth scrolling for navigation

document.querySelector('.nav__links').addEventListener('click', (e) => {
	e.preventDefault();
	// console.log(e.target);
	if (e.target.classList.contains('nav__link')) {
		const id = e.target.getAttribute('href');
		document
			.querySelector(id)
			.scrollIntoView({ behavior: 'smooth' });
	}
});

// tabbed contain
const tabsContainer = document.querySelector('.operations__tab-container'); // tab main div
const tabsBtn = document.querySelectorAll('.operations__tab'); // all button
const tabsContent = document.querySelectorAll('.operations__content'); // all content

// tabsBtn.forEach( t => t.addEventListener('click', () => console.log('click') ));
tabsContainer.addEventListener('click', (e) => {
	const clicked = e.target.closest('.operations__tab');

	if (!clicked) return;

	tabsBtn.forEach(t => t.classList.remove('operations__tab--active'));
	tabsContent.forEach(c => c.classList.remove('operations__content--active'));

	document
		.querySelector(`.operations__content--${clicked.dataset.tab}`)
		.classList.add('operations__content--active');
	clicked.classList.add('operations__tab--active');
});

// menu fade animation
const nav = document.querySelector('.nav');
const heanleHover = function (e)  {
	if ( e.target.classList.contains('nav__link') ) {
		const link = e.target;
		const siblings = link.closest('.nav').querySelectorAll('.nav__link');
		const logo = link.closest('.nav').querySelector('img');

		siblings.forEach( el => {
			if( el !== link ) el.style.opacity = this;
		});
		logo.style.opacity = this;
	}
}
nav.addEventListener('mouseover', heanleHover.bind(.5));
nav.addEventListener('mouseout', heanleHover.bind(1));

// sticky navigation 
window.addEventListener('scroll', () => {
	const navHeight = nav.getBoundingClientRect().height;
	// console.log( window.scrollY );
	if( window.scrollY >= navHeight )
		nav.classList.add('sticky');
	else 
		nav.classList.remove('sticky');
});

// load lazy image 
const imgTargets = document.querySelectorAll('img[data-src]');

const loadImg = ( entries, observer ) => {
	const [entry] = entries;

  if (!entry.isIntersecting) return;

  // Replace src with data-src
  entry.target.src = entry.target.dataset.src;

  entry.target.addEventListener('load', function () {
    entry.target.classList.remove('lazy-img');
  });

  observer.unobserve(entry.target);

}
const imgObserver = new IntersectionObserver(loadImg, { root : null, threshold : 0} );

imgTargets.forEach( img => imgObserver.observe(img) );


// slider

const slider = () => {
	const slides = document.querySelectorAll('.slide');
	const sliders = document.querySelector('.slider');
	const btnLeft = document.querySelector('.slider__btn--left');
	const btnRight = document.querySelector('.slider__btn--right');
	const dotContainer = document.querySelector('.dots');
	let currentSlide = 0;
	let maxSlide = slides.length;

	const createDots = () => {
		slides.forEach( function (_, i) {
			dotContainer.insertAdjacentHTML(
				'beforeend', 
				`<button class="dots__dot" data-slide="${i}"></button>`
			);
		});
	};

	const activateDot = ( slide ) => {
		document.
		querySelectorAll('.dots__dot')
		.forEach( dot => dot.classList.remove('dots__dot--active'));
		
		document
		.querySelector(`.dots__dot[data-slide="${slide}"]`).classList.add('dots__dot--active');
	}

	const goToSlide =  (slide) => {
		slides.forEach( (s, i) => s.style.transform = `translateX(${ 100 * ( i - slide )}%)` );
	}

	const nextSlide = () => {
		currentSlide++ ;
		if( currentSlide >= maxSlide  ) currentSlide = 0;
		goToSlide(currentSlide);
		activateDot(currentSlide);
	}

	const prevSlide =  () => {
		currentSlide-- ;
		if( currentSlide < 0 ) currentSlide = maxSlide -1 ;
		goToSlide(currentSlide);
		activateDot(currentSlide);
	}

	btnRight.addEventListener('click', nextSlide );
	btnLeft.addEventListener('click', prevSlide );

	sliders.addEventListener('dragenter', function (e) {
		console.log( e );
	});

	document.addEventListener('keydown', function(e) {
		if ( e.key === 'ArrowLeft') prevSlide();
		e.key === 'ArrowRight' && nextSlide();
	});
	dotContainer.addEventListener('click', function(e) {
		if( e.target.classList.contains('dots__dot') ) {
			const {slide} = e.target.dataset;
			goToSlide( slide );
			activateDot( slide );
		}
	});

	const sliderInit = () => {
		goToSlide(0);
		createDots();
		activateDot(0);
	}
	sliderInit();
};

slider () ;












//////////////////// practice //////////////

// console.log( document.documentElement);
// const head=document.head;
// head.prepend(`this is he`);
// console.log(head)

// selector 
// const header = document.querySelector('.header');

// // Creating and Inseting Elements

// const create_element = document.createElement('div');
// create_element.classList.add('cookie-message')
// create_element.innerHTML = 'This is headding 2 <button class="btn btn--close-cookie">Go to!</button>';

// // header.prepend(create_element) ;
// // header.append(create_element.cloneNode(true)) ;
// // header.before(create_element);
// header.after(create_element);

// // for delete element from html page

// document.querySelector('.btn--close-cookie')
// 	.addEventListener('click', () => {
// 		// create_element.remove();
// 		create_element.parentElement.removeChild(create_element);
// 	});
// create_element.style.backgroundColor = '#37383d';
// create_element.style.width = '120%';

// // for all property
// console.log(getComputedStyle(create_element));
// // for spacific property
// console.log(getComputedStyle(create_element).color);

// const increswidth = Number.parseFloat(getComputedStyle(create_element).width, 10) + 30 + 'px';
// console.log(increswidth);

// document.documentElement.style.setProperty('--color-primary', 'orange');

// // Attributes 
// const logo = document.querySelector('.nav__logo');
// logo.setAttribute('user_name', 'Gm Abbas Uddin');
// console.log(logo);
// console.log(logo.alt);
// // set alt teg
// logo.alt = "Hello! this is Gm abbas uddin";
// console.log(logo.alt);
// console.log(logo.src);
// console.log(logo.id);
// console.log(logo.className);
// console.log(logo.getAttribute('user_name'));

// // spacial attributes is "data"
// console.log(logo.dataset.versionNumber);

// // classes
// logo.classList.add('add');
// logo.classList.remove('add');
// logo.classList.toggle('add');

// const h1 = document.querySelector('h1');

// // query for child
// console.log(h1.querySelectorAll('.highlight'));
// console.log(h1.childNodes);
// console.log(h1.children);

// h1.firstElementChild.style.backgroundColor = 'red';
// h1.lastElementChild.style.backgroundColor = 'green';

// // query for parent
// console.log(h1.parentNode);
// console.log(h1.parentNode.querySelector('img'));
// console.log(h1.parentElement)

// h1.closest('.header').style.background = 'var(--gradient-secondary)';

// // sibling for before div element selector
// console.log(h1.previousElementSibling);
// // to similler ot previousElementSibling
// console.log(h1.previousSibling) 


// // sibling for after div element selector
// console.log(h1.nextElementSibling);
// // to similler ot previousElementSibling
// console.log( h1.nextSibling );

// console.log( h1.parentElement.children );
// // h1.parentElement.children.style.background = 'green';



