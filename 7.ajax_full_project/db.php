<?php

class Db{
    private $dsn = "mysql:host=localhost;dbname=r_creation";
    private $user = "root";
    private $pass = "";
    public $conn ;

    public function __construct()
    {
        try{
            $this->conn = new PDO($this->dsn,$this->user, $this->pass);
            //echo "connection";
        }catch(PDOException $error){
            echo $error->getMessage();
        }
    }

    public function store($name, $class, $roll, $reg, $html, $php, $grade)
    {
        $sql = "INSERT INTO student(std_name, std_cls_id, std_roll, std_reg, html, php, grade) VALUES (:name,:class, :roll, :reg, :html, :php, :grade)";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute(['name'=> $name, 'class'=> $class, 'roll'=> $roll, 'reg'=>$reg, 'html' => $html, 'php' => $php, 'grade' => $grade]);

        return $stmt ? true : false ;
        exit();
    }

    public function all()
    {
        $sql = "SELECT std_id, std_name, cls_name, std_roll, std_reg, html, php, grade FROM student INNER JOIN class ON std_cls_id = cls_id ORDER BY std_id DESC";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
        foreach ($results as $result) {
            $data[] = $result;
        }
        return $data;
        exit();
    }
    public function getUserById($id){

        $sql = "SELECT std_id, std_name, cls_name, std_roll, std_reg, html, php, grade FROM student INNER JOIN class ON std_cls_id = cls_id WHERE std_id = :id";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute(['id'=> $id]);
        $result = $stmt->fetch(PDO::FETCH_ASSOC);
        return $result;
        exit();
    }
    public function update($id, $name, $cls_id, $roll, $reg, $html, $php, $grade)
    {
        $sql = "UPDATE student SET std_name = :name, std_cls_id = :cls_id, std_roll = :roll, std_reg = :reg, html = :html, php = :php, grade = :grade WHERE std_id = :id";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute(['id' => $id, 'name'=> $name, 'cls_id'=> $cls_id, 'roll'=> $roll, 'reg'=>$reg, 'html' => $html, 'php' => $php, 'grade' => $grade]);
        return $stmt ? true : false ;
        exit();
    }
    public function delete($id)
    {
        $sql = "DELETE FROM student WHERE std_id = :id";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute(['id' => $id]);
        return $stmt ? true : false ;
        exit();
    }
    public function countRow()
    {
        $sql = "SELECT * FROM student";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        $row = $stmt->rowCount();
        return $stmt;
        exit();
    }
    public function classes(){
        $sql = "SELECT * FROM `class`";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
        foreach ($results as $result) {
            $data[] = $result;
        }
        return $data;
        exit();
    }
    public static function grade($data){
        if($data >= 0.00 && $data <= 4.00){
            if ($data == 4) {
                $g = 'A+';
            } else if($data >= 3.75) {
                $g = 'A';
            } else if($data >= 3.50) {
                $g = 'A-';
            } else if($data >= 3.25) {
                $g = 'B+';
            } else if($data >= 3.00) {
                $g = 'B';
            } else if($data >= 2.75) {
                $g = 'B-';
            } else if($data >= 2.50) {
                $g = 'C+';
            } else if($data >= 2.25) {
                $g = 'C';
            } else if($data >= 2.00) {
                $g = 'D';
            } else if($data >= 0.00) {
                $g = 'F';
            }
        }else{
            $g = "Wrong Input";
        }
        return $g;
    }
    public static function checker($data){
        return $data == true ? '<i class="far fa-check-circle text-success"></i>' : '<i class="far fa-times-circle text-danger"></i>';
    }
    public static function valided_data($grade){
        if($grade < 2.00 || $grade > 4.00){
            $color = 'bg-danger text-white';
        }  
        else if ($grade == 4){ 
            $color = 'bg-success text-white';
        }
        else{
            $color = '';
        }  
        return $color;
    }

}
