<?php

if (isset($_POST['action']) && $_POST['action'] == 'insert') {
    $name   = $_POST['name'];
    $class  = $_POST['class'];
    $roll   = $_POST['roll'];
    $reg    = $_POST['reg'];
    $html   = $_POST['html'];
    $php    = $_POST['php'];
    $grade  = $_POST['grade'];
    require_once "Db.php" ;
    $db = new Db();
    $db->store($name, $class, $roll, $reg, $html, $php, $grade);
}

if(isset($_POST['edit_id'])){
    $edit_id = $_POST['edit_id'];
    require_once "Db.php" ;
    $db = new Db();
    $data = $db->getUserById($edit_id);
    echo json_encode($data);
}

if (isset($_POST['action']) && $_POST['action'] == "update") {
    $id = $_POST['id'];
    $name = $_POST['name'];
    $cls_id = $_POST['class'];
    $roll = $_POST['roll'];
    $reg = $_POST['reg'];
    $html = $_POST['html'];
    $php = $_POST['php'];
    $grade = $_POST['grade'];
    require_once "Db.php" ;
    $db = new Db();
    $db->update($id, $name, $cls_id, $roll, $reg, $html, $php, $grade);
}

if (isset($_POST['delete_id'])) {
    $delete_id = $_POST['delete_id'];
    require_once "Db.php" ;
    $db = new Db();
    $db->delete($delete_id);
}

if (isset($_POST['infoId'])) {
    $id = $_POST['infoId'];
    require_once "Db.php" ;
    $db = new Db();
    $data = $db->getUserById($id);
    echo json_encode($data);
}
if(isset($_GET['action']) && $_GET['action'] == 'export'){
    header("Content-Type: application/xls");
    header("Content-Disposition: attachment; filename=student.xls");
    header("Pragma: no-cache");
    header("Expires: 0");
    require_once "Db.php" ;
    $db = new Db();
    $students = $db->all();
    include_once "table-xls.php";

}

if(isset($_GET['action']) && $_GET['action'] == 'pdf'){
    require 'pdfcrowd.php';

    try {
        $client = new \Pdfcrowd\HtmlToPdfClient("root", "200");
        $pdf = $client->convertUrl("table-xls.php");

        header("Content-Type: application/pdf");
        header("Cache-Control: no-cache");
        header("Accept-Ranges: none");
        header("Content-Disposition: attachment; filename=\"student.pdf\"");

        echo $pdf;
    }
    catch(\Pdfcrowd\Error $why) {
        fwrite(STDERR, "Pdfcrowd Error: {$why}\n");
    }
}


?>