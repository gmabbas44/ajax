<?php
    if(isset($_POST['action']) && $_POST['action'] == 'action'):
        require_once "Db.php" ;
        $i=1;
        $db = new Db();
        $students = $db->all();
        //var_dump($students);
        
?>

<table class="table table-bordered table-striped table-sm">
    <thead class="table-primary text-center">
        <tr>
            <th><input type="checkbox" name="" id="selectAll" class="checkbox"></th>
            <th>Sl</th>
            <th>Name</th>
            <th>Class</th>
            <th>Roll</th>
            <th>Reg</th>
            <th>HTML</th>
            <th>PHP</th>
            <th>Grade</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach($students as $student):?>
        <tr>
            <td class="text-center"><input type="checkbox" name="" class="checkbox" id="check"></td>
            <td class="text-center"><?= $i ;?></td>
            <td><?= $student['std_name'] ;?></td>
            <td class="text-center"><?= strtoupper($student['cls_name']) ;?></td>
            <td class="text-center"><?= $student['std_roll'] ;?></td>
            <td class="text-center"><?= $student['std_reg'] ;?></td>
            <td class="text-center"><?= Db::checker($student['html']) ;?></td>
            <td class="text-center"><?= Db::checker($student['php']) ;?></td>
            <td class="text-center <?= Db::valided_data($student['grade'])?>"><?= Db::grade($student['grade']) ;?></td>
            <td class="text-center">
                <a href="#" id="<?= $student['std_id'];?>" title="User Details" class="text-success intfo-btn"><i class="fas fa-info-circle"></i></a>
                <a href="#" id="<?= $student['std_id'];?>" data-toggle="modal" data-target="#updateStudent" title="Edit" class="text-primary edit-btn"><i class="fas fa-user-edit"></i></a>
                <a href="#" id="<?= $student['std_id'];?>" title="Delete" class="text-danger delete-btn"><i class="far fa-trash-alt"></i></a>
            </td>
        </tr>
        <?php $i++; endforeach ;?>
    </tbody>
</table>

<?php
    else:
        header('location:index.php');
    endif;
?>