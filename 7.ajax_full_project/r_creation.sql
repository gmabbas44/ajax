-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 26, 2020 at 03:01 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `r_creation`
--

-- --------------------------------------------------------

--
-- Table structure for table `class`
--

CREATE TABLE `class` (
  `cls_id` int(11) NOT NULL,
  `cls_name` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `class`
--

INSERT INTO `class` (`cls_id`, `cls_name`) VALUES
(1, 'one'),
(2, 'two'),
(3, 'three'),
(4, 'four'),
(5, 'five'),
(6, 'six'),
(7, 'seven'),
(8, 'eight'),
(9, 'nine'),
(10, 'ten');

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE `student` (
  `std_id` int(11) NOT NULL,
  `std_name` varchar(40) NOT NULL,
  `std_cls_id` int(11) NOT NULL,
  `std_roll` int(11) NOT NULL,
  `std_reg` int(11) NOT NULL,
  `html` tinytext NOT NULL,
  `php` tinytext NOT NULL,
  `grade` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`std_id`, `std_name`, `std_cls_id`, `std_roll`, `std_reg`, `html`, `php`, `grade`) VALUES
(3, 'Khaleda Akter Nodi', 10, 382616, 218616, '1', '1', 4),
(5, 'Anwer Ullah', 5, 382619, 218628, '1', '1', 4),
(6, 'Bijoy Kumar', 8, 382635, 218628, '1', '1', 4),
(8, 'Akbar Hossain', 8, 125269, 208541, '0', '0', 3),
(9, 'Anwer Ullah', 8, 382619, 218600, '1', '0', 1.89),
(11, 'Gm Abbas Uddin', 9, 741852, 987456, '0', '0', 3),
(13, 'Khadela Akter Nodi', 10, 456745, 541236, '1', '1', 4),
(15, 'MD. ABBAS UDDIN', 6, 741236, 852456, '1', '1', 2),
(17, 'Khaleda Akter Nodi', 10, 382616, 218616, '1', '1', 4),
(18, 'Nodi', 10, 123456, 987456, '1', '0', 4),
(21, 'ABBAS', 3, 122019, 253251, '0', '0', 1),
(22, 'Khaleda Akter Nodi', 9, 122019, 142563, '0', '0', 4),
(23, 'ABBAS', 9, 234, 543, '0', '0', 1),
(24, 'ABBAS', 8, 234, 345, '1', '0', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `class`
--
ALTER TABLE `class`
  ADD PRIMARY KEY (`cls_id`);

--
-- Indexes for table `student`
--
ALTER TABLE `student`
  ADD PRIMARY KEY (`std_id`),
  ADD KEY `std_cls_id` (`std_cls_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `class`
--
ALTER TABLE `class`
  MODIFY `cls_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `student`
--
ALTER TABLE `student`
  MODIFY `std_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `student`
--
ALTER TABLE `student`
  ADD CONSTRAINT `student_ibfk_1` FOREIGN KEY (`std_cls_id`) REFERENCES `class` (`cls_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
