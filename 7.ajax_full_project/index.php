<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ajax full project</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.20/datatables.min.css"/>

</head>
<body>
    
    <nav class="navbar navbar-expand-lg navbar-light bg-success ">
    <div class="container">
        <a class="navbar-brand" href="#">Navbar</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
        </button>
    </div>
    </nav>

    <div class="container mt-2">
        <div class="row">
            <div class="col-md-4">
                <h3 class="text-success">All Student </h3>
            </div>
            <div class="col-md-8">
                <button type="button" data-toggle="modal" data-target="#addStudent" class="btn btn-primary float-right"><i class="fas fa-user-plus"></i> Add Student</button>
                <a href="action.php?action=export" class="btn btn-success float-right mr-2 text-white"><i class="fas fa-file-csv"></i> Export to Excel</a>
                <a href="action.php?action=pdf" class="btn btn-success float-right mr-2 text-white"><i class="fas fa-file-csv"></i> Export to PDF</a>
            </div>
        </div>
        <!-- user add modal start -->
        <div class="modal fade" id="addStudent" tabindex="-1" role="dialog" aria-labelledby="addStudent" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="">Add New - Student</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="" id="addNewStudent">
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input class="form-control" type="text" name="name" required>
                            </div>
                            <div class="form-group">
                                <label for="class">Class</label>
                                <select class="form-control" name="class" id="class" required>
                                    <option value="">Select one</option>
                                    <!-- include class.php -->
                                </select>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md">
                                        <label for="roll">Roll</label>
                                        <input class="form-control" type="number" maxlength="6" name="roll" required>
                                    </div>
                                    <div class="col-md">
                                        <label for="reg">REG</label>
                                        <input class="form-control" type="number" maxlength="6" name="reg"required>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md">
                                        <label for="html">HTML</label>
                                        <select class="form-control" name="html" required>
                                            <option value="">Select one</option>
                                            <option value="0">No</option>
                                            <option value="1">Yes</option>
                                        </select>
                                    </div>
                                    <div class="col-md">
                                        <label for="php">PHP</label>
                                        <select class="form-control" name="php" required>
                                            <option value="">Select one</option>
                                            <option value="0">No</option>
                                            <option value="1">Yes</option>
                                        </select>
                                    </div>
                                    <div class="col-md">
                                        <label for="grade">Grade</label>
                                        <input class="form-control" type="number" maxlength="5" name="grade"required>
                                    </div>
                                </div>
                            </div>
                        
                            <div class="row">
                                <div class="col-md">
                                    <button type="button" class="btn btn-block btn-secondary" data-dismiss="modal">Close</button>
                                </div>
                                <div class="col-md">
                                    <button type="submit" id="add" class="btn btn-block btn-primary">Save</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- user add modal end -->

         <!-- user update modal start -->
         <div class="modal fade" id="updateStudent" tabindex="-1" role="dialog" aria-labelledby="updateStudent" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="">Update Student</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="" id="update_Student">
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input class="form-control" type="hidden" name="id" id="id">
                                <input class="form-control" type="text" name="name" id="name" required>
                            </div>
                            <div class="form-group">
                                <label for="class">Class</label>
                                <select class="form-control" name="class" id="update-class" required>
                                    <!-- include class.php -->
                                </select>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md">
                                        <label for="roll">Roll</label>
                                        <input class="form-control" type="number" maxlength="6" name="roll" id="roll" required>
                                    </div>
                                    <div class="col-md">
                                        <label for="reg">REG</label>
                                        <input class="form-control" type="number" maxlength="6" name="reg" id="reg" required>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md">
                                        <label for="html">HTML</label>
                                        <select class="form-control" name="html" id="html" required>
                                            <option value="">Select one</option>
                                            <option value="0">No</option>
                                            <option value="1">Yes</option>
                                        </select>
                                    </div>
                                    <div class="col-md">
                                        <label for="php">PHP</label>
                                        <select class="form-control" name="php" id="php" required>
                                            <option value="">Select one</option>
                                            <option value="0">No</option>
                                            <option value="1">Yes</option>
                                        </select>
                                    </div>
                                    <div class="col-md">
                                        <label for="grade">Grade</label>
                                        <input class="form-control" type="number" name="grade" id="grade" required>
                                    </div>
                                </div>
                            </div>
                        
                            <div class="row">
                                <div class="col-md">
                                    <button type="button" class="btn btn-block btn-secondary" data-dismiss="modal">Close</button>
                                </div>
                                <div class="col-md">
                                    <button type="submit" id="update" class="btn btn-block btn-primary">Save</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- user update modal   end /-->
    </div>

    <div class="container mt-2">
        <div class="row">
            <div class="col-md-12">
                <div id="showUser" class="table-responsive">
                    <!-- include show-user.php -->
                    <h3 class="text-center text-success" style="margin-top:150px;">Loading....</h3>
                </div>
            </div>
        </div>
    </div>
   
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/v/bs4/dt-1.10.20/datatables.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.1/js/all.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<!-- https://sweetalert2.github.io/ -->
<script>
$(document).ready(function(){


    showUser();
    
    function showUser(){
        $.ajax({
            url : "show-user.php",
            type : "post",
            data : {action : "action"},
            success:function(show){
                $('#showUser').html(show);
                $('table').DataTable({
                    "order" : [[0, 'desc']]
                });
                //console.log(show);
            }
        });
    }

    // add record
    $('#add').click(function(e){
        if($('#addNewStudent')[0].checkValidity()){
            var tr = $(this).closest('tr');
            e.preventDefault();
            $.ajax({
                url: 'action.php',
                type: 'post',
                data : $('#addNewStudent').serialize()+"&action=insert",
                success:function(response){
                    tr.css('background', '#f00').delay(1000).fadeOut();
                    swal.fire({
                        icon: 'success',
                        title : 'Student added successfully',
                        type : 'success',
                    });
                    $('#addStudent').modal('hide');
                    $('#addNewStudent')[0].reset();
                    showUser();
                }
            });
        }
    });

    // update data
    $('#update').click(function(e){
        if($('#update_Student')[0].checkValidity()){
            e.preventDefault();
            $.ajax({
                url: 'action.php',
                type: 'post',
                data : $('#update_Student').serialize()+"&action=update",
                success:function(response){
                    swal.fire({
                        icon: 'success',
                        title : 'Student update successfully',
                        type : 'success',
                    });
                    $('#updateStudent').modal('hide');
                    $('#update_Student')[0].reset();
                    showUser();
                    //console.log(response);
                }
            });
        }
    });

    classAll();
    function classAll(){
        $.ajax({
            url : "class.php",
            type : 'post',
            data : {req : "all"},
            success: function(data){
                $('#class').html(data);
                $('#update-class').html(data);
            }
        });
    }

    //Edit function
    $("body").on("click", ".edit-btn", function(e){
        e.preventDefault();
        var edit_id = $(this).attr('id');
        
        $.ajax({
            url : "action.php",
            type : "post",
            data : {edit_id : edit_id},
            success: function(response){
                data = JSON.parse(response);
                //console.log(data);
                $('#id').val(data.std_id);
                $('#name').val(data.std_name);
                $('#update-class').val(data.cls_name);
                $('#roll').val(data.std_roll);
                $('#reg').val(data.std_reg);
                $('#html').val(data.html);
                $('#php').val(data.php);
                $('#grade').val(data.grade);
            }
        });
    });
    
    // for delete ajax request
    $("body").on("click", ".delete-btn", function(e){
        e.preventDefault();
        var tr = $(this).closest('tr');
        var del_id = $(this).attr('id');
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url : "action.php",
                    type : "post",
                    data : {delete_id : del_id},
                    success: function(response){
                        tr.css('background', '#f00').delay(1000).fadeOut();
                        Swal.fire({
                            title : 'Deleted!',
                            text :'Student has been Delete Successfully!',
                            icon :'success',
                            type : 'success'
                        });
                        showUser();
                        //console.log('success');
                    }
                });
            }else{
                Swal.fire({
                    title : 'Cancelled',
                    text : 'Your imaginary file is safe',
                    icon : 'error'
                });
            }
        });
    });

    // chech for html and php true or false
    function checker(data){
        return data == 1 ? '<i class="far fa-check-circle text-success"></i>' : '<i class="far fa-times-circle text-danger"></i>';
    }

    // for first latter capital 
    function capcapitalize(d){
       return d.charAt(0).toUpperCase() + d.slice(1);
    }
    // for change grade point ro letter grade
    function grade(data){
        if(data >= 0.00 && data <= 4.00){
            if (data == 4) {
                g = 'A+';
            } else if(data >= 3.75) {
                g = 'A';
            } else if(data >= 3.50) {
                g = 'A-';
            } else if(data >= 3.25) {
                g = 'B+';
            } else if(data >= 3.00) {
                g = 'B';
            } else if(data >= 2.75) {
                g = 'B-';
            } else if(data >= 2.50) {
                g = 'C+';
            } else if(data >= 2.25) {
                g = 'C';
            } else if(data >= 2.00) {
                g = 'D';
            } else if(data >= 0.00) {
                g = 'F';
            }
        }else{
            g = "Wrong Input";
        }
        return g;
    }
    // for valided data
    function valided_data(grade){
        if(grade < 2.00 || grade > 4.00){
            color = 'bg-danger text-white';
        }else if (grade == 4){ 
            color = 'bg-success text-white font-weight-bold';
        }else{
            color = '';
        }   
        return color;
    }

    //for view sutdent information
    $('body').on('click', '.intfo-btn',function(e){
        e.preventDefault();
        var id = $(this).attr('id');
        $.ajax({
            url : "action.php",
            type : "post",
            data : {infoId : id},
            success : function(response){
                data = JSON.parse(response);
                var swal_html = '<div class="card border-0 m-0"><div class="card-body"><table class="table table-bordered table-bordered-primary"><tr><th>Name</th><td class="text-left" colspan="5">'+data.std_name+'</td></tr><tr><th>Class</th><td>'+capcapitalize(data.cls_name)+'</td><th>Roll</th><td>'+data.std_roll+'</td><th>REG</th><td>'+data.std_reg+'</td></tr><tr><th>HTML</th><td>'+checker(data.html)+'</td><th>PHP</th><td>'+checker(data.php)+'</td><th>Grade</th><td class="'+valided_data(data.grade)+'">'+grade(data.grade)+'</td></tr></table></div></div>';
                //console.log(data);
                Swal.fire({
                    title:"Student Information!", 
                    html: swal_html
                });
            }
        });
    });
   

});
</script>
</body>
</html>