<table border="1">
    <thead>
        <tr>
            <th>Sl</th>
            <th>Name</th>
            <th>Class</th>
            <th>Roll</th>
            <th>Reg</th>
            <th>HTML</th>
            <th>PHP</th>
            <th>Grade</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach($students as $student):?>
        <tr>
            <td><?= $student['std_id'] ;?></td>
            <td><?= $student['std_name'] ;?></td>
            <td><?= strtoupper($student['cls_name']) ;?></td>
            <td><?= $student['std_roll'] ;?></td>
            <td><?= $student['std_reg'] ;?></td>
            <td><?= $student['html'];?></td>
            <td><?= $student['php'] ;?></td>
            <td><?=$student['grade'] ;?></td>
        </tr>
        <?php endforeach ;?>
    </tbody>
</table>