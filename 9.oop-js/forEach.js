const forEach = (arr, callback) => {
  let i = 0;
  // while( i < arr.length) {
  //   callback(arr[i], i, arr)
  //   i++
  // }
  for (let i = 0; i < arr.length; i++) {
    callback(arr[i], i, arr);
  }
}

const array = [1, 2, 3, 4, 5];

forEach(array, (value, index, array) => {
  console.log(value, index, array);
})