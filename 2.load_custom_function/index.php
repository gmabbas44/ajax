<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script> -->
</head>
<body>
   <div class="container">
        <p id="test">This is html contain</p>
        <button class="btn btn-primary" onclick="myfun()" >Click me</button>
   </div>

   <script>
       function myfun(){
           var  req = new XMLHttpRequest();
           // function open('method', 'URL', 'asynchronous')
           req.open("GET","load.php", true);
           req.send();

           req.onreadystatechange = function (){
               if(req.readyState == 4 && req.status ==200){
                   document.getElementById('test').innerHTML = req.responseText;
               }
           }
       }
    </script>
</body> 
</html>