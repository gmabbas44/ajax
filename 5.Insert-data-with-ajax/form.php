<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
</head>

<body>
    <div class="container">
        <form id="myform" method="POST" action="insert.php">
            <div class="form-row">
                <div class="form-group col-md-4">
                    <label for="user">User name</label>
                    <input class="form-control" type="text" name="user" id="user">
                </div>

                <div class="form-group col-md-4">
                    <label for="pass">Password</label>
                    <input class="form-control" type="password" name="pass" id="pass">
                </div>
            </div>
            <button type="submit" id="submit" name="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>

    <div class="container mt-4">
        <h2>Display data</h2>
        <table class="table table-bordered text-center table-sm">
            <thead class="table-primary">
                <th>Id</th>
                <th>User Name</th>
                <th>Password</th>
            </thead>
            <tbody id="showdatatable">
            </tbody>
        </table>
    </div>


<script>
    $(document).ready(function () {
        var form = $('#myform');
        $("#submit").click(function() {
            $.ajax({
                url : form.attr('action'),
                // url : 'insert.php',
                type : 'POST',
                data : $('#myform input').serialize(),
                
                success: function(data){
                    console.log(data);
                }
            });
        });

       
        function displaydata(){
            $.ajax({
                url : 'showdata.php',
                type : 'post',

                success : function(response){

                    $("#showdatatable").html(response);

                }
            });
        }
        displaydata();
    });
</script>
</body>

</html>