<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
	<style>
		.bg-light{
			background-color: #0277ff !important;
		}
		.navbar-nav li.noti-card .nav-card{
			width: 350px;
			background-color: whitesmoke;
			box-shadow: 0px 2px 7px 0px;
			margin: 20px 20px;
			border-radius: 6px 6px 0 0;
			position: relative;
			border: 1px solid #4e73df;
			opacity: 0;
			visibility: hidden;
			transition: all 0.3s;
			position: absolute;
			top: 40px;
			right: -150px;
			z-index: 100;
			
		}
		.nav-card{
			width: 350px;
			background-color: whitesmoke;
			box-shadow: 0px 2px 7px 0px;
			margin: 20px 20px;
			border-radius: 6px 6px 0 0;
			position: relative;
			border: 1px solid #4e73df;
			*display: none;
			transition: all 0.3s;
			
		}
		.nav-card .nav-card-head{
			background: #4e73df;
			padding: 15px 15px;
			color: #fff;
			border-radius: 6px 6px 0 0;
		}
		.nav-card .nav-card-head h5 {
			margin-bottom: 0;
			text-transform: uppercase;
			font-size: 15px
		}
		.nav-card-body {
			border-bottom: 1px solid #c0c0c5;
			height: 246px;
			overflow-y: scroll;
		}
		.nav-card-body a:hover {
			text-decoration: none;
			background-color: #ffd;
		}
		.nav-card-body .user {
			padding: 15px 15px;
			display: flex;
		}
		.nav-card-body .user .user-img {
			float: left;
		}
		.nav-card-body .br {
			margin : 0;
		}
		.user-img img{
			width: 50px;
		}
		.user-msg-body {
			margin-left: 10px;
		}
		.user-msg-body .user-msg-time{
			color: #b7b9cc!important;
			font-size: 12px;
		}
		.user {
			position: relative;
		}
		.user .circle {
			width: 10px;
			height: 10px;
			border-radius: 50%;
			background-color: #4e73df;
			position: absolute;
			top: 25px;
			right: 23px;
		}
		.user-msg-body .user-msg {
			color: #3a3b45;
			font-size: 14px;
		}
		.nav-card-foor {
			padding: 10px 15px;
			border-radius: 0 0 6px 6px;
		}
		.nav-card-foor a{
			display: block;
			cursor: pointer;
			color: #b7b9cc!important;
			text-align: center;
		}
		.nav-card .box {
			width: 20px;
			height: 20px;
			background: #4e73df;
			position: absolute;
			transform: rotate(45deg);
			top: -11px;
			left: 155px;
		}
		
		/*---- class for seen notification -----*/		
		/*.active {
			background-color: #fff;
		}
		.circle {
			display: none;
		}*/
		/*---- class for seen notification/ -----*/	
		.navbar-nav li.noti-card {
			position: relative;
		}
		.navbar-nav li.noti-card:hover .nav-card {
			    opacity: 1;
    		visibility: visible
		}
	</style>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
	<div class="container">
		<a class="navbar-brand" href="#">Navbar</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
			</button>

			<div class="collapse navbar-collapse" id="navbarSupportedContent">
				<ul class="navbar-nav ml-auto">
					<li class="nav-item active">
					<a class="nav-link" href="#">Home</span></a>
					</li>
					<li class="nav-item noti-card">
					<a class="nav-link" href="#">notification</a>
						<div class="nav-card">
							<div class="box"></div>
							<div class="nav-card-head">
								<h5>Notification</h5>
							</div>
							<div class="nav-card-body">
								<a class="user" href="#">
									<div class="user-img">
										<img src="user.png" alt="">
									</div>
									<div class="user-msg-body">
										<small class="user-msg-time"> 1 second ago</small> <p class="circle"></p>
										<div class="user-msg"> this is messages, Hellow world this is messages, Hellow world</div>
									</div>
								</a>
								<hr class="br">
								<a class="user active" href="#">
									<div class="user-img">
										<img src="user.png" alt="">
									</div>
									<div class="user-msg-body">
										<small class="user-msg-time"> 1 second ago</small> <p class="circle"></p>
										<div class="user-msg"> this is messages, Hellow world this is messages, Hellow world</div>
									</div>
								</a>
								<hr class="br">
								<a class="user" href="#">
									<div class="user-img">
										<img src="user.png" alt="">
									</div>
									<div class="user-msg-body">
										<small class="user-msg-time"> 1 second ago</small> <p class="circle"></p>
										<div class="user-msg"> this is messages, Hellow world this is messages, Hellow world</div>
									</div>
								</a>
								<hr class="br">
								
							</div>
							<div class="nav-card-foor">
								<a href="#">See more notificaton</a>
							</div>
						</div>
					</li>
					<li class="nav-item">
					<a class="nav-link" href="#" tabindex="-1" aria-disabled="true">Disabled</a>
					</li>
				</ul>
			</div>
	</div>
  
</nav>
<div class="a">
	<a href="#">hover</a>



<div class="nav-card">
	<div class="box"></div>
	<div class="nav-card-head">
		<h5>Notification</h5>
	</div>
	<div class="nav-card-body">
		<a class="user" href="#">
			<div class="user-img">
				<img src="user.png" alt="">
			</div>
			<div class="user-msg-body">
				<small class="user-msg-time"> 1 second ago</small> <p class="circle"></p>
				<div class="user-msg"> this is messages, Hellow world this is messages, Hellow world</div>
			</div>
		</a>
		<hr class="br">
		<a class="user active" href="#">
			<div class="user-img">
				<img src="user.png" alt="">
			</div>
			<div class="user-msg-body">
				<small class="user-msg-time"> 1 second ago</small> <p class="circle"></p>
				<div class="user-msg"> this is messages, Hellow world this is messages, Hellow world</div>
			</div>
		</a>
		<hr class="br">
		<a class="user" href="#">
			<div class="user-img">
				<img src="user.png" alt="">
			</div>
			<div class="user-msg-body">
				<small class="user-msg-time"> 1 second ago</small> <p class="circle"></p>
				<div class="user-msg"> this is messages, Hellow world this is messages, Hellow world</div>
			</div>
		</a>
		<hr class="br">
		
	</div>
	<div class="nav-card-foor">
		<a href="#">See more notificaton</a>
	</div>
</div>
</div>


	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/d3js/5.15.0/d3.min.js"></script>
	<script>
		$(document).ready(function(){
			//alert('hello');
		});
	</script>
</body>
</html>